import axios from "axios";

import API from "../../../util/api";

export default {
  name: "Home",
  data: function () {
    return {
      route: "/home",
      glampings: [],
      booking: {
        checkin: this.getDate(0),
        checkout: this.getDate(2),
        adults: 2,
        children: 0,
      }
    };
  },
  created: async function () {
    console.log("Welcome to", this.$data.route, "!!!");
    this.getData();
  },
  methods: {
    // Render glampings
    getData: async function () {
      axios
        .get(API.url + API.endpoints.glampings)
        .then((result) => {
          console.log("Glampings retrieved. Total: ", result.data.length);
          this.glampings = result.data;
          console.log(result.data);
        })
        .catch((error) => {
          console.log("ERROR: ", JSON.stringify(error));
        });
    },
    getImageName: function (glampingName) {
      try {
        var images = require.context("../../../images/", false, /png$/);
        glampingName = "glamping " + glampingName.toLowerCase();
        glampingName = glampingName.replaceAll(" ", "-");
        return images("./" + glampingName + ".png");
      } catch (e) {
        var images = require.context("../../../images/", false, /png$/);
        return images("./errorimage.png");
      }
    },
    getFormattedPrice: function (glampingPrice) {
      return new Intl.NumberFormat("es-CO", {
        style: "currency",
        currency: "COP",
      }).format(glampingPrice);
    },
    // Render date
    getDate(days) {
      const now = new Date();
      now.setDate(now.getDate() + days);
      return `${now.getFullYear()}-${('0' + (now.getMonth() + 1)).slice(-2)}-${('0' + now.getDate()).slice(-2)}`
    },
    // Button events
    availabilityButton: function () {
      console.log(this.booking);
    },
    bookingButton: function (glampingId) {
      console.log("Reservar el glamping con ID =", glampingId);
    },
  },
};