export default {
    url: 'https://serena-glamping-main.herokuapp.com',
    // url: 'http://localhost:8000',
    endpoints: {
        users: '/api/v1/users/',
        login: '/api/v1/users/login/',
        refresh_token: '/api/v1/users/refresh/',
        glampings: '/api/v1/glampings/',
        booking: '/api/v1/booking',
    },
}