### MISION TIC 2021

# LA SERENA GLAMPING PROJECT

---

## Authors 

|#| Name | Role |
|---|---|---|
| 1 | Camilo Andrés Borda Gil | SCRUM master |
| 2 | Sandra Cristina Espinosa Ramírez | Product Owner |
| 3 | Christian Camilo Bolivar Romero | Developer |
| 4 | Willian Chadid Altamar | Developer |
| 5 | Santiago Echeverria Acosta | Developer |

---

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Font awesome packages

1. Install packages.
```
$ npm i --save @fortawesome/fontawesome-svg-core
$ npm i --save @fortawesome/free-brands-svg-icons
$ npm i --save @fortawesome/free-solid-svg-icons
$ npm i --save @fortawesome/vue-fontawesome@prerelease
```
2. Import packages on `main.js`.
```
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';
library.add(fas, fab);
```
3. Add the component to the created app on `main.js`.
```
createApp(App)
    .use(router)
    .component('font-awesome-icon', FontAwesomeIcon)
    .mount('#app')
```
4. Use the icons as:
 - `<font-awesome-icon icon="coffee" />` for *free-solid-svg-icons*.
 - `<font-awesome-icon :icon="['fab', 'facebook-square']" size="3x" />` for *free-brands-svg-icons*.