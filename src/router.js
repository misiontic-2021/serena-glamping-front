import { createRouter, createWebHashHistory } from 'vue-router';

import App from './App.vue';
// Main nav components
import Home from './components/nav/Home/Home.vue';
import Offers from './components/nav/Offers.vue';
import Gallery from './components/nav/Gallery.vue';
import AboutUs from './components/nav/AboutUs.vue';
import ContactUs from './components/nav/ContactUs.vue';
import LogIn from './components/nav/LogIn.vue';

const routes = [
  // Default route
  {
    path: '/',
    name: 'root',
    component: App,
  },
  // Main nav routes
  {
    path: '/home',
    name: 'home',
    component: Home,
  },
  {
    path: '/offers',
    name: 'offers',
    component: Offers,
  },
  {
    path: '/gallery',
    name: 'gallery',
    component: Gallery,
  },
  {
    path: '/about_us',
    name: 'about_us',
    component: AboutUs,
  },
  {
    path: '/contact_us',
    name: 'contact_us',
    component: ContactUs,
  },
  {
    path: '/login',
    name: 'login',
    component: LogIn,
  },
  //TODO add more routes
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
